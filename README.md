Sellvana Custom Checkout Login
==========================

Change default checkout login screen. Login screen is optimized for guest checkout where user is requested to enter only email address on login step. Application will validate submitted email and in case of guest checkout auto redirect customer to further steps.
In case of recognized customers, application will display customized welcoming message and offer the required password field for login.

Version:
------------------------

- Stable: alpha 0.0.1

Features:
-------------------------

- Initially request only email address on checkout login step
- Validates submitted email recognizing if it's known email or guest
- Preset and hide email fields for checkout addresses, in case of guest checkout
- Database scheme is not altered in any way.

Installation:
-------------------------


1. Clear the store cache under storage/cache and all cookies for your store domain.
2. Backup your store database and web directory.
3. Download and unzip extension contents on your computer and navigate inside the extracted folder.
4. Using your FTP client upload content of “local” directory to “local” directory inside your store root.

Activation:
-------------------------

1. Log in to Sellvana admin area.
2. Navigate to Modules->Menage Modules page.
3. Filter for module which name equals to Vuleticd_CheckoutLogin.
4. Select the module and Change Status to Run Level = REQUESTED.

Deactivation:
-------------------------

1. Log in to Sellvana admin area.
2. Navigate to Modules->Menage Modules page.
3. Filter for module which name equals to Vuleticd_CheckoutLogin.
4. Select the module and Change Status to Run Level = DISABLED.


Uninstall:
-------------------------

- You can safely remove the extension files from your store.

