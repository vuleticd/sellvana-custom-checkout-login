<?php
/*
# NOTICE OF LICENSE
#
# This source file is subject to the Open Software License (OSL 3.0)
# that is available through the world-wide-web at this URL:
# http://opensource.org/licenses/osl-3.0.php
#
# @category    Vuleticd
# @package     Vuleticd_CheckoutLogin
# @copyright   Copyright (c) 2014 Vuletic Dragan (http://www.vuleticd.com)
# @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/
class Vuleticd_CheckoutLogin_Frontend_Observer
{
    /*
    * On guest checkout preset customer address email with customer email found in cart
    */
    public static function onAfterCreate($args)
    {
        if(!FCom_Customer_Model_Customer::i()->isLoggedIn()){
            $address = $args['model'];
            $cart = FCom_Sales_Model_Cart::i()->sessionCart();
            $address->set('email', $cart->get('customer_email'));
        }
    }
}