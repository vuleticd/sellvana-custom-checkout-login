<?php
/*
# NOTICE OF LICENSE
#
# This source file is subject to the Open Software License (OSL 3.0)
# that is available through the world-wide-web at this URL:
# http://opensource.org/licenses/osl-3.0.php
#
# @category    Vuleticd
# @package     Vuleticd_CheckoutLogin
# @copyright   Copyright (c) 2014 Vuletic Dragan (http://www.vuleticd.com)
# @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/
?>
<?php defined('BUCKYBALL_ROOT_DIR') || die();

class Vuleticd_CheckoutLogin_Frontend_Controller_Checkout extends FCom_Frontend_Controller_Abstract
{
    public function action_validate__POST()
    {
        $r    = $this->BRequest;
        $post = $r->post();
        try {
            $customerModel = $this->FCom_Customer_Model_Customer;
            $user = $customerModel->orm()->where('email', $post['login']['email'])->find_one();
            if (!$user){
                // guest - new email
                $cart = $this->FCom_Sales_Model_Cart->sessionCart();
                $cart->set('customer_email', $post['login']['email']);
                $cart->save();

                $this->BResponse->json(['status' => 'success', 'redirect' => $post['redirect_to_guest']]);
            } else {
                // customer found
                $this->BResponse->json(['status' => 'success', 'firstname' => $user->firstname, 'redirect' => $post['redirect_to_customer'] ]);
            }
        } catch (Exception $e) {
            $this->BResponse->json(['status' => 'error', 'message' => $e->getMessage()]);
        }
        
    }
}
