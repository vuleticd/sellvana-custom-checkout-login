/*
# NOTICE OF LICENSE
#
# This source file is subject to the Open Software License (OSL 3.0)
# that is available through the world-wide-web at this URL:
# http://opensource.org/licenses/osl-3.0.php
#
# @category    Vuleticd
# @package     Vuleticd_CheckoutLogin
# @copyright   Copyright (c) 2014 Vuletic Dragan (http://www.vuleticd.com)
# @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

define(["jquery", "vuleticd.checkout.login"], function ($) {
	var loginForm = $('#login-form');
	loginForm.on('click', 'button', function(ev) {
		loginForm.validate();
		//step 1
		if ($('#login-password').hasClass('hidden')) {
			if (loginForm.valid()) {
				$.ajax({
					type: "POST",
					url: loginForm.attr('action'),
					data: loginForm.serialize(),
					success: function (data){
						loginForm.find('label.error').remove();
						if(data.status == 'success'){
							if(data.firstname){
								// if it's customer enable step 2 and change form submittion
								$('#login-password').removeClass('hidden');
								$('#username').html(data.firstname);
								$('#password').addClass('required');
								loginForm.attr('action', data.redirect);
							} else{
								// if guest redirect forward
								window.location = data.redirect;
							}
						} else {
							var label = '<label for="email" class="error">' + data.message + '</label>';
							$('#email').after(label);
						}
					},
					error: function (xhr, textStatus, errorThrown){
						alert("[ERROR]<br>" + textStatus);
					}
				});
			}
			ev.preventDefault();
			ev.stopPropagation();
			return false;
		}
	}) 
});