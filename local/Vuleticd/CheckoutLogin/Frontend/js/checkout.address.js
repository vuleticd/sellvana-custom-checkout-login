/*
# NOTICE OF LICENSE
#
# This source file is subject to the Open Software License (OSL 3.0)
# that is available through the world-wide-web at this URL:
# http://opensource.org/licenses/osl-3.0.php
#
# @category    Vuleticd
# @package     Vuleticd_CheckoutLogin
# @copyright   Copyright (c) 2014 Vuletic Dragan (http://www.vuleticd.com)
# @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

define(["jquery", "vuleticd.checkout.address"], function ($) {
	var addressEmail = $('#address-email');
	if(addressEmail.val()){
		var row = addressEmail.parents( ".row" );
		row.addClass('hidden');
	}
});